<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MeetingController extends Controller
{
    public function store(Request $request){

    	$this->validate($request,[
    		'title'=>'required',
    		'description'=>'required',
    		'time'=>'required',
    		'user_id'=>'required',
    	]);
    	
    	$title = $request->input('title');
        $description = $request->input('description');
        $time= $request->input('time');
        $user_id = $request->input('user_id');
      
        $meeting = [
            'title' => $title,
            'description' => $description,
            'time' => $time,
            'user_id' => $user_id,
            'view_meeting' => [
            	'href' => 'api/v1/meeting',
            	'method' => 'GET'
            ]
        ];
        return response()->json(
            $response=[
            'success' => true,
            'msg'=> 'Meeting Created',
            'data' => $meeting
        ],200);
    }
}
